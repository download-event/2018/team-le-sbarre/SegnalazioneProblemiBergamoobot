-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Set 09, 2018 alle 13:20
-- Versione del server: 5.6.33-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_lesbarre`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `segnalazioni`
--

CREATE TABLE IF NOT EXISTS `segnalazioni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipologia` varchar(100) NOT NULL,
  `luogo` varchar(100) NOT NULL,
  `descrizione` text NOT NULL,
  `userid` int(11) NOT NULL,
  `ora_segnalazione` datetime NOT NULL,
  `link_Immagine` varchar(200) NOT NULL,
  `completata` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dump dei dati per la tabella `segnalazioni`
--

INSERT INTO `segnalazioni` (`id`, `tipologia`, `luogo`, `descrizione`, `userid`, `ora_segnalazione`, `link_Immagine`, `completata`) VALUES
(21, 'illuminazione', 'si trova in via giuseppe garibaldi', 'ho trovato un lampione che non funziona', 33, '2018-09-09 14:02:12', ' ', 1),
(20, 'strada', 'in via iv maggio', 'ho trovato una buca', 32, '2018-09-09 14:02:12', ' ', 1),
(19, 'pianta', 'di fronte allo stadio', 'ho trovato una pianta', 30, '2018-09-09 14:02:12', ' ', 1),
(18, 'strada', 'ho trovato un pianta per terra', 'ho trovato una buca', 29, '2018-09-09 14:02:12', ' ', 0),
(17, 'strada', 'di fronte allo stadio', 'ho trovato una buca', 29, '2018-09-09 14:02:12', ' ', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE IF NOT EXISTS `utenti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `userid` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`id`, `nome`, `cognome`, `email`, `userid`) VALUES
(29, 'domenico', 'gaeni', 'domenicogaeni@gmail.com', '232022581'),
(30, 'numa', 'pelli', 'fjdnjfndjnfd@fdjnf', '232022581'),
(31, 'fabio', 'cava', 'fndjfnjdnf@gmail.com', '232022581'),
(32, 'paolo', 'donini', 'fhdjhfbdhbfh', '232022581'),
(33, 'mario', 'fiorini', 'dkfnkdnfndk', '232022581');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
