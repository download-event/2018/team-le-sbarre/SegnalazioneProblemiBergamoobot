<?php
  require 'connetti.php';
  $opzioni = array('Pianta','Strada','Illuminazione');
  $corrente=$opzioni[0];
  if($_SERVER['REQUEST_METHOD'] == 'POST')
  {
    $corrente = $_POST['tipo'];
  }
  $query="SELECT * FROM segnalazioni WHERE tipologia='$corrente' AND completata='0' ORDER BY id,ora_segnalazione DESC";
  $tabella = mysqli_query($conn, $query);
?>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <title>La sbarre | Segnalazioni di problemi al Comune</title>
  <script>
    function controlla()
    {
      document.getElementById('myForm').submit();
    }
    function conferma(num)
    {
        if(confirm("Sei sicuro di voler completare questa segnalazione. Il cittadino che ha segnalato il problema riceverà un messaggio che lo informa che la sua segnalazione è stata completata"))
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                alert(this.responseText);
                location.reload();
              }
            };
            xhttp.open("GET", "confermaOperazione.php?id="+num, true);
            xhttp.send();
        }     
    }  
    function commenta(num)
    {
      	var messaggio = prompt("Inserisci il messaggio che vuoi inviare al cittadino", "Invia messaggio");
        if(messaggio!="")
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                alert(this.responseText);                
              }
            };
            xhttp.open("GET", "mandaMessaggio.php?id="+num+"&mex="+messaggio, true);
            xhttp.send();
        }  
    
    }
  </script>
</head>
<body>
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4">Le sbarre</h1>
      <p class="lead">Domenico Gaeni, Luca Maccarini, Fabio Cavaleri e Aaron Tognoli</p>
    </div>
  </div>
  <div class="container">
    <form id="myForm" method="post" action="./">
      <div class="form-group row">
        <label for="selezioneTipo" class="offset-sm-2 col-sm-4 col-form-label">Tipologia:</label>
        <div class="col-sm-4">
          <select class="form-control" onchange="controlla()" name="tipo" id="selezioneTipo">
            <?php
                for ($i=0; $i < count($opzioni); $i++)
                {
                  if(strtolower($opzioni[$i])==$corrente)
                    echo "<option selected value='".strtolower($opzioni[$i])."'>".$opzioni[$i]."</option>";
                  else
                    echo "<option value='".strtolower($opzioni[$i])."'>".$opzioni[$i]."</option>";
                }
            ?>
          </select>
        </div>
      </div>
    </form>
  </div>
  <hr><br><br>
  <div class="container">
  	<div class="table-responsive">
      <table class="table">
        <?php


        if(mysqli_num_rows($tabella)>0)
        {
          echo "<thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Descrizione</th>
              <th scope='col'>Luogo</th>
              <th scope='col'>Utente</th>
              <th scope='col'>Data e ora</th>
              <th scope='col'>Operazioni</th>
            </tr>
          </thead>
           <tbody>";
           $conta=0;
          while ($riga = $tabella->fetch_assoc())
          {
            $conta++;
            echo "<tr>";
            echo "<td>$conta</td>";
            echo "<td>".$riga['descrizione']."</td>";
            echo "<td>".$riga['luogo']."</td>";
            $app=$riga['userid'];
            $idapp=$riga['userid'];
            $query = "SELECT * FROM utenti WHERE id='$app'";
            $risu = mysqli_query($conn, $query);
            $r=mysqli_fetch_assoc($risu);
            $app=$r['cognome'] . " " .$r['nome'];
            echo "<td>".$app."</td>";
            $data = date("d-m-Y H:i:s", strtotime($riga['ora_segnalazione']));
            echo "<td>".$data."</td>";
            echo "<td style='text-align:center; font-size:22px;'><i class='fas fa-check' title='Completata' onclick='conferma($idapp)'></i> <i class='fas fa-times' title='Rispondi...' onclick='commenta($idapp)'></i></td>";
            echo "</tr>";
           	            
          }
          echo "</tbody>";
        }
        else
          echo "<h2>Non è presente alcuna segnalazione</h2>";
        ?>

      </table>
    </div>
  </div>
</body>
